import sys, time, os, shutil, glob

F_REG = """Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\%s]
@="Nobody cares"
"URL Protocol"=""
"UseOriginalUrlEncoding"=dword:00000001

[HKEY_CLASSES_ROOT\%s\shell]

[HKEY_CLASSES_ROOT\%s\shell\open]

[HKEY_CLASSES_ROOT\%s\shell\open\command]
@="%s %s"
"""

F_C = """#include <unistd.h>

int main(int argc, char** argv){
	return execlp("python3", "python3", "%s", argv[1], NULL);
}
"""

F_RC = """1 VERSIONINFO
BEGIN
  BLOCK "StringFileInfo"
  BEGIN
    BLOCK "080904E4"
    BEGIN
      VALUE "FileDescription", "%s"
    END
  END
  BLOCK "VarFileInfo"
  BEGIN
    VALUE "Translation", 0x809, 1252
  END
END

2 ICON ico.ico
"""

def main():
	if len(sys.argv) < 3:
		print("""Usage:
		python3 setup.py <proto name> <title>""")
		return
	# path to our dir
	pth = os.path.dirname(os.path.realpath(sys.argv[0]))
	os.chdir(pth)
	# get args
	proto = sys.argv[1]
	title = sys.argv[2]
	# variate name of exe for no chrome cache
	exe = 'main_%d.exe' % time.time()
	# ecranize
	def sl(s):
		return "\\\"%s\\\"" % s.replace('\\', '\\\\')
	# remove old exe
	[os.remove(f) for f in glob.glob('*.exe')]
	# work in temp dir
	os.chdir(pth)
	try:
		os.mkdir('temp')
	except:
		pass
	os.chdir(pth + os.path.sep + 'temp')
	# write reg file
	with open('main.reg', 'w', encoding='utf8') as f:
		f.write(F_REG % (
			proto, proto, proto, proto,
			sl(pth + os.path.sep + exe),
			sl("%1")
		))
	# write c file
	with open('main.c', 'w', encoding='utf8') as f:
		f.write(F_C % sl(pth + os.path.sep + 'main.py'))
	# write rc file
	with open('main.rc', 'w', encoding='1251') as f:
		f.write(F_RC % title)
	# copy icon
	shutil.copy('..' + os.path.sep + 'ico.ico', 'ico.ico')
	# compile exe
	if os.system('windres main.rc -O coff -o main.res') != 0:
		return
	if os.system('gcc main.c main.res -o main.exe') != 0:
		return
	# accept reg
	os.system('main.reg')
	# save exe
	shutil.copy('main.exe', '..' + os.path.sep + exe)
	# remove temp dir
	os.chdir(pth)
	shutil.rmtree('temp')

main()
