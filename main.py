import sys, os
from urllib.parse import unquote

def main():
	os.chdir(os.path.dirname(os.path.realpath(sys.argv[0])))
	url = unquote(sys.argv[1].split(':', 1)[1])
	handle(url)

def handle(url):
	with open('log.txt', 'a', encoding='utf8') as f:
		f.write(url)
		f.write('\n')

main()
